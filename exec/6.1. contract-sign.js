function SignContract(accessToken, contractToken, envelope, status) {
    if(typeof envelope != 'undefined') {
        SignContractData.envelopeId = envelope;
    }

    if(typeof status != 'undefined') {
        SignContractData.status = status;
    }

    $.ajax({
        type: "POST",
        url: base + "/client/contract/status/" + contractToken,
        data: JSON.stringify(SignContractData),
        headers: {
            'username': 'usrdocusign',
            'password': 'Us3rd0cus1gn',
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function(data) {
            document.write("/client/contract/status > " + data.status + "<BR>");

            ConfirmContract(accessToken);
        }
    });
}