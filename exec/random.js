function nextDNI() {
    var letters = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
    var number = Math.floor(Math.random() * 100000000);
    var letter = letters[number % 23];


    if(number <= 999)
        return "00000" + number + letter;
    else if(number <= 9999)
        return "0000" + number + letter;
    else if(number <= 99999)
        return "000" + number + letter;
    else if(number <= 999999)
        return "00" + number + letter;
    else if(number <= 9999999)
	    return "0" + number + letter;
	else
		return number + letter;
}

function nextNIE() {
    var DNI = nextDNI();

    switch (DNI.charAt(0)) {
        case '0':
            return "X" + DNI.slice(1);
        case '1':
            return "Y" + DNI.slice(1);
        case '2':
            return "Z" + DNI.slice(1);
        default:
            return nextNIE();
    }
}

function nextCNO() {
    var cnos = $.ajax({
        type: "GET",
        dataType: 'json',
        url: base + "/profession/cno",
        async: false
    }).responseText;

    cnos = jQuery.parseJSON(cnos);

    var result;
    var count = 0;

    for (var prop in cnos.items) {
        if (Math.random() < 1/++count)
           result = cnos.items[prop].id;
    }

    return result;
}

function nextCNAE() {

    var cnaes = $.ajax({
        type: "GET",
        dataType: 'json',
        url: base + "/profession/cnae",
        async: false
    }).responseText;

    cnaes = jQuery.parseJSON(cnaes);

    var result;
    var count = 0;

    for (var cat in cnaes.items) {
        if (Math.random() < 1/++count) {
            for (var item in cnaes.items[cat].items) {
                if (Math.random() < 1/++count)
                    result = cnaes.items[cat].items[item].id;
                }
            }
    }
    
    return result;
}

function nextUnmployed() {
    var unemployed = ["X200A", "X300B", "X300A"];
    var rand = Math.floor(Math.random() * (2 - 0 + 1)) + 0;

    return unemployed[rand];
}