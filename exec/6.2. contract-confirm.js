function ConfirmContract(accessToken) {
    $.ajax({
        type: "POST",
        url: base + "/client/contract/confirm/",
        headers: {
            'Authorization': 'Bearer ' + accessToken,
            'sessionToken': accessToken,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function(data) {
            document.write("/client/contract/confirm > " + data.status + "<BR>");
            document.write("<HR>");

            DocumentationList(accessToken);
        }
    });
}