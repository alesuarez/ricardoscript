function Register() {
    $.ajax({
        type: "POST",
        url: base + "/client/register",
        data: JSON.stringify(RegisterData),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        
        success: function(data) {
            document.write("/client/register > " + data.status + "<BR>");
            document.write(" * name: " + data.name + "<BR>");
            document.write(" * lastnames: " + data.lastname1 + " " + data.lastname2 + "<BR>");
            document.write(" * document: " + data.docType + " " + data.docNumber + "<BR>");
            document.write(" * iban: " + data.iban + "<BR>");
            document.write(" * access token: " + data.sessionToken + "<BR>");
            document.write(" * refresh token: " + data.refreshToken + "<BR><BR>");

            Additional(data.sessionToken);
        }
    });
}