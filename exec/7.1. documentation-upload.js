var Base64 = "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACklEQVR4nGNiAAAABgADNjd8qAAAAABJRU5ErkJggg==";


function DocumentationUpload(accessToken) {
    if(IdentificationMethod == "iban") {
      UploadFront(accessToken);
    } else {
      UploadDaal(accessToken);
    }
}

function UploadFront(accessToken) {
    switch(RegisterData.docType) {
        case "DNI":
            var FrontData = {
              "file": Base64,
              "fileType": "dni_front",
              "filename": "dni_front.png"
            };
            var BackData = {
              "file": Base64,
              "fileType": "dni_back",
              "filename": "dni_back.png"
            };
            break;
        case "NIE":
            var FrontData = {
              "file": Base64,
              "fileType": "nie_front",
              "filename": "nie_front.png"
            };
            var BackData = {
              "file": Base64,
              "fileType": "passport_front",
              "filename": "passport_front.png"
            };
            break;
        case "PDR":
            var FrontData = {
              "file": Base64,
              "fileType": "permit_front",
              "filename": "permit_front.png"
            };
            var BackData = {
              "file": Base64,
              "fileType": "permit_back",
              "filename": "permit_back.png"
            };
            break;
    }

    $.ajax({
        type: "POST",
        url: base + "/client/documentation/upload",
        data: JSON.stringify(FrontData),
        headers: {
            'Authorization': 'Bearer ' + accessToken,
            'sessionToken': accessToken,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function(data) {
            document.write("/client/documentation/upload (" + FrontData.fileType + ") > " + data.status + "<BR>");
            UploadBack(accessToken, BackData);
        }
    });
}

function UploadBack(accessToken, BackData) {
    $.ajax({
        type: "POST",
        url: base + "/client/documentation/upload",
        data: JSON.stringify(BackData),
        headers: {
            'Authorization': 'Bearer ' + accessToken,
            'sessionToken': accessToken,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function(data) {
            document.write("/client/documentation/upload (" + BackData.fileType + ") > " + data.status + "<BR>");
            
            UploadDaal(accessToken);
        }
    });
}

function UploadDaal(accessToken) {
    if(FiocPath == "unemployed") {
        CheckDocumentationList(accessToken);
        return;
    }

    var DaalData = {
      "file": Base64,
      "fileType": "daal",
      "filename": "daal.png"
    }; 

    $.ajax({
        type: "POST",
        url: base + "/client/documentation/upload",
        data: JSON.stringify(DaalData),
        headers: {
            'Authorization': 'Bearer ' + accessToken,
            'sessionToken': accessToken,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function(data) {
            document.write("/client/documentation/upload (" + DaalData.fileType + ") > " + data.status + "<BR>");

            CheckDocumentationList(accessToken);
        }
    });
}

function CheckDocumentationList(accessToken) {
    document.write("<HR>");

    $.ajax({
      type: "GET",
      url: base + "/client/documentation",
      headers: {
          'Authorization': 'Bearer ' + accessToken,
          'sessionToken': accessToken,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      },
      success: function(data) {
          document.write("/client/documentation > " + data.status + "<BR>");
          document.write(" * account type: " + data.accountType + "<BR>");
          document.write(" * doc list: " + "<BR>");

          $.each(data.documents, function(index, doc) {
              document.write(" ** " + 
                  doc.docId + " (" + 
                  doc.status + ")"+ "<BR>");
          });
          
          DocumentationConfirm(accessToken);
      }
  });
}