function DocumentationList(accessToken) {
    $.ajax({
        type: "GET",
        url: base + "/client/documentation",
        headers: {
            'Authorization': 'Bearer ' + accessToken,
            'sessionToken': accessToken,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function(data) {
            document.write("/client/documentation > " + data.status + "<BR>");
            document.write(" * account type: " + data.accountType + "<BR>");
            document.write(" * doc list: " + "<BR>");

            $.each(data.documents, function(index, doc) {
            	document.write(" ** " + 
            		doc.docId + " (" + 
            		doc.status + ")"+ "<BR>");
			});

            DocumentationUpload(accessToken);
        }
    });
}
