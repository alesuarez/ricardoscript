function Identification(accessToken) {
    $.ajax({
        type: "POST",
        url: base + "/client/identification/courier",
        data : JSON.stringify(IdentificationData),
        headers: {
            'Authorization': 'Bearer ' + accessToken,
            'sessionToken': accessToken,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function(data) {
            document.write("/client/identification/courier > " + data.status + "<BR>");
            document.write(" * identification token: " + data.identificationToken + "<BR>");

            CompleteCourier(accessToken, data.identificationToken);
        }
    });
}