function Create(accessToken, password) {
    var CreateData = {
        "password": password
    };

    $.ajax({
        type: "POST",
        url: base + "/client/create",
        data: JSON.stringify(CreateData),
        headers: {
            'Authorization': 'Bearer ' + accessToken,
            'sessionToken': accessToken,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        
        success: function(data) {
            document.write("/client/create > " + data.status + "<BR>");
            document.write(" * persona: " + data.tipoPersona + " " + data.codigoPersona + "<BR>");

            Identification(accessToken);
        }
    });
}