function DocumentationConfirm(accessToken) {
    $.ajax({
        type: "GET",
        url: base + "/client/documentation/confirm",
        headers: {
            'Authorization': 'Bearer ' + accessToken,
            'sessionToken': accessToken,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function(data) {
            document.write("/client/documentation/confirm > " + data.status + "<BR>");
        }
    });
}
