var FiocPath = "rentier";
var FiocData = 
{
  "capital": {
    "activities": false,
    "business": false,
    "heritage": false,
    "other": "capital",
    "realState": true,
    "work": false
  },
  "income": {
    "activities": false,
    "business": false,
    "other": "Ingresos actuales",
    "realState": false,
    "work": false
  },
  "annualIncome":true,
  "operations": {
    "buyingAndSellingForeingBanknotes": false,
    "buyingSellingBearerPaymentMethods": false,
    "deliveryOrCashWithdrawals": false,
    "foreingTradeOperations": true,
    "noneOfTheAbove": false,
    "paymentOrdersOrCollectionAbroad": false
  },
  "publicCharges": {
    "answer": true,
    "details": "Parent president"
  },
  "purpose": {
    "investment":true
  },
  "taxation": {
    "answer": true,
    "citizenship": true,
    "country": "AR",
    "residency": true,
    "tin": "tin"
  },
  "thirdParty": true
}