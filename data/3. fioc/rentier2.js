var FiocPath = "rentier";
var FiocData = 
{
  "sectors": {
    "noneOfTheAbove": null,
    "militaryProducts": null,
    "paymentsOrCurrencyExchange": null,
    "casinosOrBettingEntities": null,
    "importExportTelecommunications": null,
    "importExportAutomobile": null,
    "importExportElectronic": null,
    "jewelryPreciousStonesAndMetals": null,
    "recyclingMaterials": null,
    "cashHandling": null,
    "worksOfArtOrAntiques": null,
    "userCarsDealer": null
  },
  "income": {
    "activities": false,
    "business": false,
    "realState": false,
    "work": true,
    "other": null
  },
  "capital": {
    "activities": false,
    "business": false,
    "realState": false,
    "work": true,
    "other": null,
    "heritage": false
  },
  "annualIncome": false,
  "thirdParty": false,
  "operations": {
    "buyingSellingBearerPaymentMethods": false,
    "deliveryOrCashWithdrawals": false,
    "buyingAndSellingForeingBanknotes": false,
    "paymentOrdersOrCollectionAbroad": false,
    "foreingTradeOperations": false,
    "noneOfTheAbove": false
  },
  "publicCharges": {
    "answer": false,
    "details": null
  },
  "purpose": {
    "personal": true,
    "investment": null,
    "professional": null
  },
  "taxation": {
    "answer": false,
    "citizenship": null,
    "residency": null,
    "country": null,
    "tin": null
  }
}