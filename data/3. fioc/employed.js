var FiocPath = "employed";
var FiocData = 
{
  "annualIncome": true,
  "capital": {
    "activities": false,
    "business": true,
    "heritage": false,
    "other": "capital",
    "realState": true,
    "work": false
  },
  "companyName": "globant",
  "income": {
    "activities": false,
    "business": true,
    "other": "income",
    "realState": false,
    "work": true
  },
  "operations": {
    "buyingAndSellingForeingBanknotes": true,
    "buyingSellingBearerPaymentMethods": false,
    "deliveryOrCashWithdrawals": true,
    "foreingTradeOperations": false,
    "noneOfTheAbove": true,
    "paymentOrdersOrCollectionAbroad": false
  },
  "profession": {
    "description": "Enologos",
    "id": "2427",
    "option": "1",
    "sector": "privado",
    "subcategory": "HOSTELERIA / TURISMO"
  },
  "publicCharges": {
    "answer": false,
    "details": "pep"
  },
  "purpose": {
    "investment": true
  },
  "taxation": {
    "answer": true,
    "taxationDetails": [
      {
        "citizenship": true,
        "country": "AR",
        "residency": false,
        "tin": "123456AR"
      },
      {
        "citizenship": true,
        "country": "AL",
        "residency": true,
        "tin": "123456AL"
      }
    ]
  },
  "thirdParty": true
}