var FiocPath = "ownaccount";
var FiocData = 
{
  "annualIncome": false,
  "capital": {
    "other": "Alquier Apartamento",
    "work": true
  },
  "companyName": "Globant",
  "income": {
    "activities": true,
    "business": true
  },
  "operations": {
    "buyingSellingBearerPaymentMethods": true,
    "deliveryOrCashWithdrawals": true
  },
  "profession": {
    "id": "A0161",
    "option": "No",
    "subcategory": "1"
  },
  "publicCharges": {
    "answer": true,
    "details": "Ministro de educacion"
  },
  "purpose": {
    "personal": true, 
    "other": false, 
    "professional": false
  },
  "sectors": {
    "category" : "1",
    "sector" : "103"
  },
  "taxation": {
    "answer": false
  },
  "thirdParty": true
}