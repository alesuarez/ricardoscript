var FiocPath = "unemployed";
var FiocData = 
{
  "capital": {
    "activities": false,
    "business": true,
    "heritage": false,
    "other": "capital",
    "realState": true,
    "work": false
  },
  "income": {
    "activities": false,
    "business": true,
    "other": "income",
    "realState": false,
    "work": true
  },
  "operations": {
    "buyingAndSellingForeingBanknotes": true,
    "buyingSellingBearerPaymentMethods": false,
    "deliveryOrCashWithdrawals": true,
    "foreingTradeOperations": false,
    "noneOfTheAbove": true,
    "paymentOrdersOrCollectionAbroad": false
  },
    "profession": "X200A",
  "publicCharges": {
    "answer": true,
    "details": "pep"
  },
  "purpose": {
    "personal": true
  },
  "taxation": {
    "answer": false
  },
  "thirdParty": false
}