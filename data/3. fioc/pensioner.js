var FiocPath = "pensioner";
var FiocData = 
{
  "capital": {
    "activities": false,
    "business": true,
    "heritage": false,
    "other": "capital",
    "realState": true,
    "work": false
  },
  "income": {
    "activities": false,
    "business": true,
    "other": "income",
    "realState": false,
    "work": true
  },
  "operations": {
    "buyingAndSellingForeingBanknotes": true,
    "buyingSellingBearerPaymentMethods": false,
    "deliveryOrCashWithdrawals": true,
    "foreingTradeOperations": false,
    "noneOfTheAbove": true,
    "paymentOrdersOrCollectionAbroad": false
  },
  "publicCharges": {
    "answer": true,
    "details": "pep"
  },
  "purpose": {
    "personal": true
  },
  "taxation": {
    "answer": true,
    "citizenship": true,
    "country": "AR",
    "residency": true,
    "tin": "tin"
  },
  "thirdParty": false
}